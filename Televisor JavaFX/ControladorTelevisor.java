import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javax.swing.JOptionPane;
import javafx.scene.image.ImageView;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;
import javafx.application.Application; 
import javafx.scene.Scene; 
import javafx.scene.layout.*; 
import javafx.event.ActionEvent; 
import javafx.event.EventHandler; 
import javafx.scene.control.*; 
import javafx.stage.Stage; 
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TextInputDialog;

public class ControladorTelevisor {

    @FXML
    private BorderPane panel1;

    @FXML
    private Button cmdSubir;

    @FXML
    private Button cmdEntrada;

    @FXML
    private Label lblTitulo;

    @FXML
    private Label lblVolumen;
    
    @FXML
    private ImageView imageTV;
    
    @FXML
    private Label lblEntrada;

    @FXML
    private GridPane panelDatos;
    
    @FXML
    private TextField txtVolumen;

    @FXML
    private Button cmdBajar;

    @FXML
    private Button cmdRetroceder;

    @FXML
    private Label lblCanal;

    @FXML
    private TextField txtCanal;

    @FXML
    private Button cmdAvanzar;

    @FXML
    private GridPane panelBotones;

    @FXML
    private TextField txtEntrada;
    
    private ModeloTelevisor televisor;
    
    public ControladorTelevisor(){
    televisor = new ModeloTelevisor();
    }
    
    @FXML
    void subirVolumen() {
    televisor.subirVolumen();
    txtVolumen.setText(Integer.toString(televisor.getVolumen()));
    }

    @FXML
    void bajarVolumen() {
    televisor.bajarVolumen();
    txtVolumen.setText(Integer.toString(televisor.getVolumen()));
    }

    @FXML
    void avanzarCanal() {
    televisor.avanzarCanal();
    txtCanal.setText(Integer.toString(televisor.getCanal()));
    }

    @FXML
    void retrocederCanal() {
    televisor.retrocederCanal();
    txtCanal.setText(Integer.toString(televisor.getCanal()));
    }

    @FXML
    void cambiarEntrada() {
    List<String> choices = new ArrayList<>();
    choices.add("Apagado");
    choices.add("Antena");
    choices.add("Cable");
    choices.add("Auxiliar");
    
    ChoiceDialog<String> dialog = new ChoiceDialog<>("Apagado", choices);
    dialog.setTitle("null");
    dialog.setHeaderText("Seleccione el Tipo de Entrada");
    dialog.setContentText("Tipo de Entrada:");
    
    Optional<String> result = dialog.showAndWait();
    
    if(result.get() == "Apagado"){
    televisor.setTipoDeEntrada(0);
    }
    if(result.get() == "Antena"){
    televisor.setTipoDeEntrada(1);
    }
    if(result.get() == "Cable"){
    televisor.setTipoDeEntrada(2);
    }
    if(result.get() == "Auxiliar"){
    televisor.setTipoDeEntrada(3);
    }
        
        
    /*String opcion = (String)JOptionPane.showInputDialog(
                    null,
                    "Tipo de Entrada TV",
                    "Seleccione el tipo de entrada y pulse aceptar",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    televisor.tiposDeEntrada,
                    televisor.tiposDeEntrada[1]);
     for(int i=0; i<televisor.tiposDeEntrada.length; i++)
      if(televisor.tiposDeEntrada[i].equals(opcion))
        televisor.setTipoDeEntrada(i);*/
     
    txtCanal.setText(Integer.toString(televisor.getCanal()));
    txtEntrada.setText(televisor.getTipoDeEntrada());
    txtVolumen.setText(Integer.toString(televisor.getVolumen()));
    }
}
